import copy


class Tetrimino:
    """ Tetriminos are stored as relative coordinates. """
    blocktypes = [
        [(0, 0), (0,  -1), (0,  -2), (0,  -3)],
        [(0, 0), (0,  -1), (0,  -2), (1,  -2)],
        [(0, 0), (0,  -1), (1,  -1), (1,  -2)],
        [(0, 0), (0,  -1), (1,  -1), (2,  -1)],
        [(0, 0), (-1, -1), (0,  -1), (1,  -1)],
        [(0, 0), (0,  -1), (-1, -1), (-1, -2)],
        [(0, 0), (1,   0), (0,  -1), (1,  -1)]
    ]

    def __init__(self, _type):
        self.type = _type
        self.coords = self.blocktypes[_type][:]  # this is a shallow copy, but tuples are immutable

    def __str__(self):
        """ Currently does not work when rotated more than once"""
        l = [['_' for _ in range(4)] for _ in range(4)]
        for i, (y, x) in enumerate(self.coords):
            l[x+3][y+1] = str(i)
        return '\n'.join(''.join(row) for row in l[::-1])

    def rotate(self):
        if self.type == 6:
            return self
        else:
            self.coords = [(y, -x) for x, y in self.coords]
            return self

    def add(self, x0, y0):
        """ Returns the coordinates of the tetrimino placed on the board at (x0, y0). """
        return [(x0+x, y0+y) for x, y in self.coords]

    @classmethod
    def test(cls):
        blocks = [cls(i) for i in range(len(cls.blocktypes))]
        print('### Block types')
        for i, block in enumerate(blocks):
            print('Type', i)
            print(block, '\n')
        print('### Rotated')
        for block in blocks:
            print(block.rotate(), '\n')


class TetrisState:
    def __init__(self, width=8, height=18):
        self.board = [[bool() for _ in range(width)] for _ in range(height)]
        """
        The game board is represented as a 2 dimensional bool array,
        where the origin is at the top left.
        Outer index is the row number, so indexing is [y][x] """

    @property
    def height(self):
        return len(self.board)

    @property
    def width(self):
        return len(self.board[0])

    def get_next_states(self, tetrimino):
        ret = []
        for x in range(self.width):
            y = max(y for y in range(self.height) if not self.board[y][x])
            newcoords = tetrimino.add(x, y)
            while not self.can_put(newcoords):
                y -= 1
                newcoords = tetrimino.add(x, y)
                if min(y for _, y in newcoords) < 0:
                    break
            else:
                ret.append(self.put(newcoords))
        return ret

    def get_next_states_rotated(self, tetrimino):
        return (self.get_next_states(tetrimino) + self.get_next_states(tetrimino.rotate()) +
                self.get_next_states(tetrimino.rotate()) + self.get_next_states(tetrimino.rotate()))

    def can_put(self, coords):
        for x, y in coords:
            if x < 0 or x >= self.width or y < 0 or y >= self.height or self.board[y][x]:
                return False
        return True

    def put(self, coords):
        ret = TetrisState(self.width, self.height)
        ret.board = copy.deepcopy(self.board)
        for x, y in coords:
            ret.board[y][x] = True
        return ret

    def game_over(self):
        return any(self.board[0])

    def remove_full_rows(self):
        fullrow = [True]*self.width
        if fullrow not in self.board:
            return False
        while fullrow in self.board:
            self.board.remove(fullrow)
            self.board = [[False]*self.width] + self.board
        return True

    def count_full_rows(self):
        return sum(all(row) for row in self.board)

    def __str__(self):
        return '\n'.join(''.join(map(str, map(int, line))) for line in self.board)

    @classmethod
    def test(cls):
        game = cls()
        for i in range(len(Tetrimino.blocktypes)):
            tetrimino = Tetrimino(i)
            for x in range(4):
                for state in game.get_next_states(tetrimino):
                    print(str(state))
                    print(input())
                tetrimino.rotate()


def main():
    TetrisState.test()
    # game = TetrisState()
    # for _ in range(10):
    #     tetrimino = Tetrimino(random.randint(0, 6))
    #     game = game.get_next_states(tetrimino)[0]
    #     print(game, input())


if __name__ == '__main__':
    main()


doc_blocktypes = """
BLOCK TYPES
Type 0:
[0]
[1]
[2]
[3]

Type 1:
[0]
[1]
[2][3]

Type 2:
[0]
[1][2]
   [3]

Type 3:
[0]
[1][2][3]

Type 4:
   [0]
[1][2][3]

Type 5:
   [0]
[2][1]
[3]

Type 6:
[0][1]
[2][3]

"""