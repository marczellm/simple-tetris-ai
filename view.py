import tkinter as tk


class TetrisView(tk.Frame):
    def __init__(self, width, height, parent=tk.Tk()):
        tk.Frame.__init__(self, parent)
        self.pack()
        self.scale = 20
        self.offset = 2
        self.canvas = tk.Canvas(parent, height=(height * self.scale) + self.offset,
                                        width = (width * self.scale) + self.offset)
        self.canvas.pack()
        self.board = [[bool() for _ in range(width)] for _ in range(height)]

    def drawBoard(self, board, color=None):
        for y, row in enumerate(board):
            for x, block in enumerate(row):
                if block:
                    rx = (x * self.scale) + self.offset
                    ry = (y * self.scale) + self.offset
                    self.canvas.create_rectangle(rx, ry, rx+self.scale, ry+self.scale, fill=color)

    def clear(self):
        self.canvas.delete(tk.constants.ALL)
        width = len(self.board[0])
        height = len(self.board)
        self.board = [[bool() for _ in range(width)] for _ in range(height)]

    def draw(self, tetrisstate, color=None):
        self.drawBoard(self.board, 'black')
        self.drawBoard(tetrisstate.board, color)
        self.board = tetrisstate.board


