from time import sleep
import random
from threading import Thread
from model import TetrisState, Tetrimino
from view import TetrisView
import player

class App:
    def __init__(self):
        self.state = TetrisState(12)
        self.view = TetrisView(self.state.width, self.state.height)
        self.agent = player.greedy
        self.speed_ms = 100
        self.view.after(self.speed_ms, self.mainloop)
        self.view.mainloop()

    def mainloop(self):
        tetrimino = Tetrimino(random.randint(0, 6))
        states = self.state.get_next_states_rotated(tetrimino)
        self.state = self.agent(states)
        self.view.draw(self.state)
        if self.state.remove_full_rows():
            self.view.clear()
            self.view.draw(self.state, 'black')
        if not self.state.game_over():
            self.view.after(self.speed_ms, self.mainloop)
        else:
            self.view.after(self.speed_ms, self.view.quit)


if __name__ == '__main__':
    App()
