import random
from heuristics import combined


def rand(states):
    return states[random.randrange(len(states))]


def first(states):
    return states[0]


def greedy(states):
    hasfull = {state: state.count_full_rows() for state in states}
    if any(hasfull.values()):
        return next(state for state, cfr in hasfull.items() if cfr == max(hasfull.values()))
    else:
        d = {state: combined(state) for state in states}
        return next(state for state, heur in d.items() if heur == min(d.values()))

