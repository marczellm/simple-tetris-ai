from model import TetrisState


def maxheight(state: TetrisState):
    if state.game_over():
        return state.height
    else:
        return state.height - max(y for y, row in enumerate(state.board) if not any(row))


def fullcells(state: TetrisState):
    return sum(sum(row) for row in state.board)


def fullcells_weighted(state: TetrisState):
    return sum((state.height-y)*sum(row) for y, row in enumerate(state.board))


def holes(state: TetrisState):
    """ Actually half-holes, meaning that there is something above """
    ret = 0
    for y, row in enumerate(state.board):
        for x, block in enumerate(row):
            above = (state.board[dy][x] for dy in range(y))
            if not block and any(above):
                ret += 1
    return ret


def slopes(state):
    """ Differences between adjacent column heights """
    heights = []
    for x in range(state.width):
        fulls = [y for y in range(state.height) if state.board[y][x]]
        if len(fulls) > 0:
            heights.append(state.height - min(fulls))
        else:
            heights.append(0)
    #heights = [state.height - min(y for y in range(state.height) if state.board[y][x]) for x in range(state.width)]
    return (abs(heights[i + 1] - heights[i]) for i in range(len(heights) - 1))


def combined(state: TetrisState):
    slops = list(slopes(state))
    return 10*holes(state) + maxheight(state) + 5*max(slops) + sum(slops) + fullcells_weighted(state)