### Simple Tetris AI

This AI was written as a university assignment for an Artificial Intelligence course. I only had around 2 days to do it, so I wrote it in Python, and it turned out surprisingly simple to write.

You don't need anything to run it except for Python 3.
